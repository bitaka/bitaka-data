# Bitaka Data

This project contains an example flashcard deck to be used with the software of the Bitaka project.

## What means Bitaka?

Bitaka comes from the Arabic word بطاقة which means "card".

## License

All data published in this project is released into the Public Domain (CC0 1.0).
You can find more information in [the license file](LICENSE.md).
